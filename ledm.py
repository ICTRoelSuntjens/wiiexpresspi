# ----------------------------------------
# @title: ---- Led Manager ---------------
# @author: --- created by Roel Suntjens --
# @date: ----- created at 07/01/2016 -----
# @version: -- v1.0 ----------------------
# ----------------------------------------

import wiiset
import wiringpi2 as pi
import time

def init():
  try:
    pi.wiringPiSetup()
    pi.pinMode( wiiset.GREENLEDPIN, pi.GPIO.OUTPUT )
    pi.pinMode( wiiset.ORANGELEDPIN, pi.GPIO.OUTPUT )
    pi.pinMode( wiiset.REDLEDPIN, pi.GPIO.OUTPUT )
    Ready( False )
  except:
    return False
  finally:
    return True

def Ready(on):
  if ( on ) :
    pi.digitalWrite( wiiset.GREENLEDPIN, pi.GPIO.HIGH )
  else :
    pi.digitalWrite( wiiset.GREENLEDPIN, pi.GPIO.LOW )

def Warning(stayOn):
  prestate = pi.digitalRead( wiiset.ORANGELEDPIN )
  pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.LOW )
  for index in range( 3 ):
    pi.digitalWrite( wiiset.ORANGELEDPIN, pi.GPIO.HIGH )
    time.sleep( .5 )
    pi.digitalWrite( wiiset.ORANGELEDPIN, pi.GPIO.LOW )
    time.sleep( .5 )  
  if ( stayOn ):
    pi.digitalWrite( wiiset.ORANGELEDPIN, pi.GPIO.HIGH )
  else:
    pi.digitalWrite( wiiset.ORANGELEDPIN, prestate )

def Error(stayOn):
  prestate = pi.digitalRead( wiiset.REDLEDPIN )
  pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.LOW )
  for index in range( 10 ):
    pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.HIGH )
    time.sleep( .2 )
    pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.LOW )
    time.sleep( .2 )
  if ( stayOn ):
    pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.HIGH )
  else:
    pi.digitalWrite( wiiset.REDLEDPIN, prestate )

def AllOff():
  pi.digitalWrite( wiiset.GREENLEDPIN, pi.GPIO.LOW )
  pi.digitalWrite( wiiset.ORANGELEDPIN, pi.GPIO.LOW )
  pi.digitalWrite( wiiset.REDLEDPIN, pi.GPIO.LOW )
