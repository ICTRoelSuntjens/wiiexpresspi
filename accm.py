# ----------------------------------------
# @title: ---- Accelerometer Manager -----
# @author: --- created by Roel Suntjens --
# @date: ----- created at 04/01/2016 -----
# @version: -- v1.0 ----------------------
# ----------------------------------------

import wiiset

def init():
  wiiset.ACC[ 'x' ] = 0
  wiiset.ACC[ 'y' ] = 0
  wiiset.ACC[ 'z' ] = 0
  return True

def handleAcc( acc ):
  wiiset.PREACC[ 'x' ] = wiiset.ACC[ 'x' ]
  wiiset.PREACC[ 'y' ] = wiiset.ACC[ 'y' ]
  wiiset.PREACC[ 'z' ] = wiiset.ACC[ 'z' ]

  wiiset.ACC[ 'x' ] = acc[ 0 ]
  wiiset.ACC[ 'y' ] = acc[ 1 ]
  wiiset.ACC[ 'z' ] = acc[ 2 ]

def isChanged():
  if ( wiiset.PREACC[ 'x' ] != wiiset.ACC[ 'x' ] ):
    return True
  elif ( wiiset.PREACC[ 'y' ] != wiiset.ACC[ 'y' ] ):
    return True
  elif ( wiiset.PREACC[ 'z' ] != wiiset.ACC[ 'z' ] ):
    return True
  return False
