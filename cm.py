# ----------------------------------------
# @title: ---- Cloud Manager -------------
# @author: --- created by Roel Suntjens --
# @date: ----- created at 04/01/2016 -----
# @version: -- v1.0 ----------------------
# ----------------------------------------

import logm as log
import wiiset
import json, httplib
import urllib2

API_ROOT = 'api.parse.com'
API_SUB = '/1/classes/'
API_PORT = 443
ACCESS_KEYS = {}
HEADERS = {}

def init():
  try:
    urllib2.urlopen( "http://google.com" )
  except urllib2.URLError, e:
    return False
  register(wiiset.APP_ID, wiiset.REST_KEY, wiiset.MASTER_KEY)
  return True

# @info: This method will register the keys that are used to send data to parse.
# @info: The keys are available in wiiset.py
def register(app_id, rest_key, master_key):
  global ACCESS_KEYS
  ACCESS_KEYS = {
	'app_id': app_id,
	'rest_key': rest_key,
	'master_key': master_key,
	'app_type': "application/json",
	}
  global HEADERS
  HEADERS = {
	"X-Parse-Application-Id": ACCESS_KEYS['app_id'],
	"X-Parse-REST_API_Key": ACCESS_KEYS['rest_key'],
	"X-Parse-Master-Key": ACCESS_KEYS['master_key'],
	"Content-Type": ACCESS_KEYS['app_type'],
  }

# @info: Sending all the wii data to parse.
# @info: This method does the same as 'sendLastWiiData()' only to an other location.
# @info: This method is to log all the wii data.
def sendAllData():
  sendJson(getConnection(), API_SUB + 'WiiData', getAllJson(), HEADERS)
  sendLastWiiData()

# @info: Sending all the eik data to parse.
# @info: This method does the same as 'sendLastEikData()' only to an other location.
# @info: This method is to log all the eik data.
def sendEikData():
  sendJson(getConnection(), API_SUB + 'WiiEikAcc', getEikJson(), HEADERS)
  sendLastEikData()

def sendLastWiiData():
  sendJsonByID(getConnection(), API_SUB + 'LatestWiiData', wiiset.LATESTDATA_KEY, getAllJson(), HEADERS)

def sendLastEikData():
  sendJsonByID(getConnection(), API_SUB + 'LatestWiiData', wiiset.LATESTEIK_KEY, getEikJson(), HEADERS)

def sendJson(connection, classesline, jsonfile, headers):
  try:
    connection.request('POST', classesline, jsonfile, headers)
  except KeyboardInterrupt:
    return
  except:
    log.Error( 'Check Parse Keys in wiiset.py' )

def sendJsonByID(connection, classesline, id, jsonfile, headers):
  try:
    connection.request('PUT', classesline + '/' + id, jsonfile, headers)
  except KeyboardInterrupt:
    return
  except:
    log.Error( 'Check Parse Keys in wiiset.py', True )

def getConnection():
  try:
    connection = httplib.HTTPSConnection(API_ROOT, API_PORT)
    connection.connect()
    return connection
  except:
    quit()

# @info: This method collects only the wiimote eik data that is stored in the wiiset.py
# @return: The method will return a json file.
def getEikJson():
  eik = wiiset.EIKACC
  jsonfile = json.dumps({
	"eikx": eik['x'],
	"eiky": eik['y'],
	"eikz": eik['z']
  })
  return jsonfile

# @info: This method collects all the wiimote data that is stored in the wiiset.py.
# @return: The method wil return a json file.
def getAllJson():
  bool = wiiset.BOOLS
  acc = wiiset.ACC
  pos = wiiset.POS
  jsonfile = json.dumps({
	"button1": bool[wiiset.BTN_1],
	"button2": bool[wiiset.BTN_2],
	"buttonA": bool[wiiset.BTN_A],
	"buttonB": bool[wiiset.BTN_B],
	"buttonMin": bool[wiiset.BTN_MIN],
	"buttonHome": bool[wiiset.BTN_HOME],
	"buttonPlus": bool[wiiset.BTN_PLUS],
	"buttonUp": bool[wiiset.BTN_UP],
	"buttonDown": bool[wiiset.BTN_DOWN],
	"buttonLeft": bool[wiiset.BTN_LEFT],
	"buttonRight": bool[wiiset.BTN_RIGHT],
	"accx": acc['x'],
	"accy": acc['y'],
	"accz": acc['z'],
	"pos1x": pos['pos1x'],
	"pos1y": pos['pos1y'],
	"size1": pos['size1'],
	"pos2x": pos['pos2x'],
	"pos2y": pos['pos2y'],
	"size2": pos['size2'],
  })
  return jsonfile
