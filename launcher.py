# ----------------------------------------
# @title: ---- Launcher ------------------
# @author: --- created by Roel Suntjens --
# @date: ----- created at 04/01/2016 -----
# @version: -- v1.0 ---------------------- 
# ----------------------------------------

#   -- Auto Login Setup (Optional) --
# -> sudo nano /etc/inittab
#  @ Scroll down till 1:2345:respawn:/sbin/getty 115200 tty1
#  @ The 115200 could be differend
# -> Add a '#' befor this line
#  @ Now it will look like this:
# #1:2345:respawn:/sbin/getty 115200 tty1
#  @ Under the line add this following line:
# -> 1:2345:respawn:/bin/login -f pi tty </dev/tty1 >/dev/tty1 2>&1
#  @ Where 'pi' is the username!
#  @ Save the document
# -> Ctrl + X
# -> Press 'Y' key
# -> Hit 'Enter'
#   -- END --

#   -- Auto Start Script (Optional) --
# -> sudo nano /etc/profile
#  @ Scroll to the bottom and add the following line:
# -> sudo python /home/pi/{replace with location}/wiiexpresspi/launcher.py
#  @ Save the document
# -> Ctrl + X
# -> Press 'Y' key
# -> Hit 'Enter'
#   -- END --

## Libraries ##
import os
import cwiid
import time

## Local ##
import accm
import bm
import cm
import irm
import ledm
import logm as log
import wiiset


def clearDisplay():
  os.system('clear')

def ledLoop(wm):
  cycle1 = [1, 2, 4, 8, 4, 2, 1, 2, 4, 8, 4, 2, 1]
  cycle2 = [1, 3, 6, 15, 6, 3, 1, 3, 6, 15, 6, 3, 1]
  cycle3 = [9, 6, 9, 6, 9, 6, 5, 10, 5, 10, 5, 10]
  for index in cycle1:
    wm.led = index
    time.sleep(.2)
  for index in cycle2:
    wm.led = index
    time.sleep(.2)
  for index in cycle3:
    wm.led = index
    time.sleep(.2)

def init():
  value = 0
  succes = False
  if (wiiset.PRINTINIT ) :
    log.FillLine()
    log.Message('INIT RASPBERRY')
  succes = False | log.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Log Manager')
    log.PrintState(succes)
  succes = False | ledm.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Led Manager')
    log.PrintState(succes)
  succes = False | accm.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Acc Manager')
    log.PrintState(succes)
  succes = False | bm.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Button Manager')
    log.PrintState(succes)
  succes = False | cm.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Cloud Manager')
    log.PrintState(succes)
  if ( not succes ) :
    ledm.AllOff()
    log.Error( 'No Internet Connection', True )
    log.Error( 'Check cable or router', True )
  succes = False | irm.init()
  if not succes : value += 1
  if ( wiiset.PRINTINIT ) | ( not succes ) :
    log.Message('Infra Red Manager')
    log.PrintState(succes)

  log.FillLine()
  return value


def initWiimote(wm):
  if wiiset.PRINTINIT:
    log.FillLine()
    log.Message( 'INIT WIIMOTE' )
    log.Message( 'Rumble' )
  if ( wiiset.RUMBLETEST == 1 ):
    if wiiset.PRINTINIT : log.Message( 'Test' )
    for i in range( 3 ):
      wm.rumble = True
      time.sleep( .5 )
      wm.rumble = False
  if ( wiiset.LED ):
    if wiiset.PRINTINIT : log.Message( 'Led' )
    if ( wiiset.LEDTEST == 1 ):
      #wait till finnished blinking
      if ( wiiset.RUMBLETEST == 0 ): time.sleep( 2.5 ) 
      if wiiset.PRINTINIT : log.Message( 'Test' )
      ledLoop( wm )
    if ( wiiset.LED ): wm.led = 1
  if ( wiiset.BATTERY ):
    battery = wm.state[ 'battery' ]
    if ( battery < 25 ):
      print wm.state
      log.Error( 'Charge your wiimote or get an other [' + str(battery) + ']', False )
      time.sleep( 5 )
      wiiset.disconnectWiimote = True
    elif ( battery < 50 ):
      print wm.state
      log.Warning( 'Charge your wiimote [' + str(battery) + ']', False )
    else:
      if wiiset.PRINTINIT : log.Message( 'Battery is fine [' + str(battery) + ']' )
  log.FillLine()
  time.sleep( 1 )

def loop( wm ):
  anyChanges = False
  button = 0
  acc = None
  ir = None
  while True:
  # - Start of loop - #
    if wiiset.disconnectWiimote :
      wiiset.disconnectWiimote = False
      log.Message ( 'Disconnect wiimote' )
      time.sleep( 2 )
      break
    button = wm.state[ 'buttons' ]
    acc = wm.state[ 'acc' ]
    ir = wm.state[ 'ir_src' ]
    if button != 0:
      bm.handleButton( button, acc )
      time.sleep( wiiset.DELAY[ 'button' ] )
    if acc != None:
      accm.handleAcc(acc)
      time.sleep( wiiset.DELAY[ 'acc' ] )
    if ir != None:
      irm.handleIr( ir )
      time.sleep( wiiset.DELAY[ 'ir' ] )

    anyChanges |= bm.isChanged()
    anyChanges |= accm.isChanged()
    anyChanges |= irm.isChanged()

    if anyChanges:
      cm.sendAllData()
      bm.resetButtons()
      irm.resetIR()
      anyChanges = False
  # - End of loop - #


def removePycFiles():
  filelist = [ f for f in os.listdir( wiiset.PATH_TO_DIR ) if f.endswith(".pyc") ]
  for f in filelist:
    os.remove( wiiset.PATH_TO_DIR + f )

def main():
  clearDisplay()
  print "for auto startup and auto run script see header 'launcher.py'"
  print "\n"


  log.FillLineColor( wiiset.GREEN )
  log.MessageColor( 'Start up Raspberry Pi Application', wiiset.GREEN )
  log.FillLineColor( wiiset.GREEN )
  while True:
  # - Start of main - #
    time.sleep( .5 )
    if ( ( init() > 0 ) & ( wiiset.STARTUPCOUNTER < wiiset.STARTUPMAXCOUNT ) ) :
      wiiset.STARTUPCOUNTER += 1
      log.Warning( '{' + str( wiiset.STARTUPCOUNTER ) + '/' + str( wiiset.STARTUPMAXCOUNT ) + '} Trying again...', True )
      continue
    elif ( wiiset.STARTUPCOUNTER >= wiiset.STARTUPMAXCOUNT ) :
      log.Error( 'Time out', True )
      removePycFiles()
      break;
    wm = None
    i = 0
    log.FillLine()
    log.MessageMiddle( 'When the green led is on: ' )
    log.Message( 'Press 1 + 2 on your Wiimote now...' )
    ledm.Ready( True )
    while not wm:
      try:
        if( i % 5 == 0 ):
          log.Warning( 'attempts left: ' + str( 10 - i ), False )
        try:
          wm = cwiid.Wiimote()
          log.Message( 'Connection Succesfull' )
        except RuntimeError:
          i += 1
          if ( i >= 10 ):
            ledm.Ready(False)
            log.Error( 'Time out', True )
            removePycFiles()
            ledm.AllOff()
            return 0

      except KeyboardInterrupt:
        log.Message( 'Keyboard Interrupt' )
        removePycFiles()
        ledm.AllOff()
        return 0
    initWiimote(wm)
    if wiiset.disconnectWiimote :
      wiiset.disconnectWiimote = False
      log.Message ( 'Disconnect wiimote' )
      time.sleep( 2 )
      continue

    wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC | cwiid.RPT_IR
    ledm.Ready( False )
 
    print '\n'
    log.FillLine()
    log.MessageMiddle( 'Instructions' )
    log.FillLine()
    log.Message( 'Hold + & - to Disconnect Wiimote' )
    log.Message( 'Hold 1 & 2 to Eik Wiimote' )
    log.FillLine()
    print '\n'

    loop( wm )
    clearDisplay()

  # - End of main - #

if __name__ == '__main__':
  main()
