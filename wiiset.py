# -----------------------------------------
# @title: ---- Wiimote Settings -----------
# @author: --- created by Roel Suntjens ---
# @date: ----- 04/01/2016 -----------------
# @version: -- v1.0 -----------------------
# @info: ----- Here are all de settings ---
# @info: ----- of this project ------------
# -----------------------------------------

BLUE = '\033[34;1m'
GREEN = '\033[32;1m'
WARNING = '\033[33;1m'
FAILED = '\033[31;1m'
ENDC = '\033[0m'

TODO = "!!! Still to implement !!!"

# -- Rest Api settings --
APP_ID = "YGUDFfarP0MFXnoHlAdOee2OcfZ01QI4YgxisyNa"
REST_KEY = "lrJv1I4Ow2jqgeR5Ovo8HObcMFCbCEVzazhYCBQL"
MASTER_KEY = "7z0YdmBUP0m1DkYWaV0vzbirMEzatf8QlFhIKPdm"

# -- Parse Keys --
# @parse.com. If class LatestWiiData is empty:
# add two rows to the collection and copy these keys below.
LATESTDATA_KEY = "NnXXGgjrRs"
LATESTEIK_KEY = "Wjg6VXbkRK"

# -- Raspberry settings --
PATH_TO_DIR = "/home/pi/git/wiiexpresspi/"
RUMBLETEST = 0
LEDTEST = 0
RUMBLE = 0
LED = 1
BATTERY = 1
PRINTINIT = 1
PRINTLOG = 1
STARTUPCOUNTER = 0
STARTUPMAXCOUNT = 5

# -- Const --
BTN_1 = 2
BTN_2 = 1
BTN_A = 8
BTN_B = 4
BTN_MIN = 16
BTN_HOME = 128
BTN_PLUS = 4096
BTN_UP = 2048
BTN_DOWN = 1024
BTN_LEFT = 256
BTN_RIGHT = 512

BTN_1_2 = 3
BTN_A_B = 12
BTN_MIN_PLUS = 4112

DELAY = {
	'button': 0.0,
	'acc': 0.0,
	'ir': 0.0,
}

GREENLEDPIN = 4
ORANGELEDPIN = 5
REDLEDPIN = 6

# -- Variable --
BOOLS =	{
	BTN_1: 0,
	BTN_2: 0,
	BTN_A: 0,
	BTN_B: 0,
	BTN_MIN: 0,
	BTN_HOME: 0,
	BTN_PLUS: 0,
	BTN_UP: 0,
	BTN_DOWN: 0,
	BTN_LEFT: 0,
	BTN_RIGHT: 0,
	}

ACC = {
	'x': 0,
	'y': 0,
	'z': 0,
}
PREACC = {
	'x': 0,
	'y': 0,
	'z': 0,
}
EIKACC = {
	'eikx': 0,
	'eiky': 0,
	'eikz': 0,
}
PREPOS = {
	'1x': 0,
	'1y': 0,
	's1': 0,
	'2x': 0,
	'2y': 0,
	's2': 0,
	'3x': 0,
	'3y': 0,
	's3': 0,
	'4x': 0,
	'4y': 0,
	's4': 0,
}
POS = {
	'pos1x': 0,
	'pos1y': 0,
	'size1': 0,
	'pos2x': 0,
	'pos2y': 0,
	'size2': 0,
	'pos3x': 0,
	'pos3y': 0,
	'size3': 0,
	'pos4x': 0,
	'pos4y': 0,
	'size4': 0,
}

disconnectWiimote = False

preEikTime = 0
curEikTime = 0
eiking = False
eikCountDown = 0
eikCounter = 3

def IsEiking():
  return eiking & BOOLS[ BTN_1 ] == 1 & BOOLS[ BTN_2 ] == 1
