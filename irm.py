# ----------------------------------------
# @title: ---- Infra Red Manager ---------
# @author: --- created by Roel Suntjens --
# @date: ----- created at 04/01/2016 -----
# @version: -- v1.0 ----------------------
# ----------------------------------------

import wiiset

def init():
  return True

def handleIr( ir ):
  for idx in range(4) :
    if ir[ idx ] != None :
      index = str( idx + 1 )
      wiiset.PREPOS[ index + 'x' ] = wiiset.POS[ 'pos' + index + 'x' ]
      wiiset.PREPOS[ index + 'y' ] = wiiset.POS[ 'pos' + index + 'x' ]
      wiiset.PREPOS[ 's' + index ] = wiiset.POS[ 'size' + index ]
      
      wiiset.POS[ 'pos' + index + 'x' ] = ir[ idx ][ 'pos' ][ 0 ]
      wiiset.POS[ 'pos' + index + 'y' ] = ir[ idx ][ 'pos' ][ 1 ]
      wiiset.POS[ 'size' + index ] = ir[ idx ][ 'size' ]

def isChanged():
  pp = wiiset.PREPOS
  p = wiiset.POS
  for idx in range( 1, 5 ) :
    if ( ( pp[ str( idx ) + 'x' ] != p[ 'pos' + str( idx ) + 'x'] ) | ( pp[ str( idx ) + 'y' ] != p[ 'pos' + str( idx ) + 'y' ] ) | ( pp[ 's' + str( idx ) ] != p[ 'size' + str( idx ) ] ) ):
      return True
  return False

def resetIR():
  for idx in range( 1, 5 ) :
    wiiset.POS[ 'pos' + str( idx ) + 'x' ] = 0
    wiiset.POS[ 'pos' + str( idx ) + 'y' ] = 0
    wiiset.POS[ 'size' + str( idx ) ] = 0
