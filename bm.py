# ----------------------------------------
# @title: ---- Button Manager ------------
# @author: --- created by Roel Suntjens --
# @date: ----- created at 04/01/2016 -----
# @version: -- v1.0 ----------------------
# ----------------------------------------

import cm
import logm as log
import wiiset
from datetime import datetime, time

def init():
  wiiset.preEikTime = getCurrentTimeInSec()
  wiiset.curEikTime = getCurrentTimeInSec()
  return True

def one():
  wiiset.BOOLS[ wiiset.BTN_1 ] = 1

def two():
  wiiset.BOOLS[ wiiset.BTN_2 ] = 1

def a():
  wiiset.BOOLS[ wiiset.BTN_A ] = 1

def b():
  wiiset.BOOLS[ wiiset.BTN_B ] = 1

def up():
  wiiset.BOOLS[ wiiset.BTN_UP ] = 1

def down():
  wiiset.BOOLS[ wiiset.BTN_DOWN ] = 1

def left():
  wiiset.BOOLS[ wiiset.BTN_LEFT ] = 1

def right():
  wiiset.BOOLS[ wiiset.BTN_RIGHT ] = 1

def min():
  wiiset.BOOLS[ wiiset.BTN_MIN ] = 1

def home():
  wiiset.BOOLS[ wiiset.BTN_HOME ] = 1

def plus():
  wiiset.BOOLS[ wiiset.BTN_PLUS ] = 1

def oneTwo( acc ):
  wiiset.BOOLS[ wiiset.BTN_1 ] = 1
  wiiset.BOOLS[ wiiset.BTN_2 ] = 1
  if ( not wiiset.IsEiking() ) :
    log.Message ( 'Eiking Wiimote...' )
    wiiset.eiking = True
    wiiset.curEikTime = getCurrentTimeInSec()
    wiiset.eikCountDown = wiiset.eikCounter
  else :
    if( wiiset.eikCountDown > 0 & wiiset.preEikTime < wiiset.curEikTime ):
      log.Message( 'Hold ' + str( wiiset.eikCountDown ) + ' seconds...' )
      wiiset.preEikTime = wiiset.curEikTime
      wiiset.eikCountDown -= 1
    else :
      eikData( acc )
      wiiset.eiking = False

def aB():
  wiiset.BOOLS[ wiiset.BTN_A ] = 1
  wiiset.BOOLS[ wiiset.BTN_B ] = 1

def minPlus():
  wiiset.BOOLS[ wiiset.BTN_MIN ] = 1
  wiiset.BOOLS[ wiiset.BTN_PLUS ] = 1
  wiiset.disconnectWiimote = True

def eikData(acc):
  wiiset.EIKACC[ 'x' ] = acc[ 0 ]
  wiiset.EIKACC[ 'y' ] = acc[ 1 ]
  wiiset.EIKACC[ 'z' ] = acc[ 2 ]
  log.PrintState( True )
  cm.sendEikData()

def getCurrentTimeInSec():
  utcnow = datetime.utcnow()
  midnight_utc = datetime.combine(utcnow.date(), time( 0 ))
  delta = utcnow - midnight_utc
  return delta.seconds

functions = {
	wiiset.BTN_A_B: aB,
	wiiset.BTN_MIN_PLUS: minPlus,
	}
accfunctions = {
	wiiset.BTN_1_2: oneTwo,
	}
buttons={
	wiiset.BTN_1: one,
	wiiset.BTN_2: two,
	wiiset.BTN_A: a,
	wiiset.BTN_B: b,
	wiiset.BTN_UP: up,
	wiiset.BTN_DOWN: down,
	wiiset.BTN_RIGHT: right,
	wiiset.BTN_LEFT: left,
	wiiset.BTN_MIN: min,
	wiiset.BTN_HOME: home,
	wiiset.BTN_PLUS: plus,
	}

def handleButton( button, acc ):
  try:
    functions[ button ]()
  except KeyError:
    try:
      accfunctions[ button ]( acc )
    except KeyError:
      try:
        buttons[ button ]()
      except KeyError:
        log.Message( 'Unknown button [' + str( button ) + ']' )

def isChanged():
  bool = wiiset.BOOLS
  if bool[ wiiset.BTN_A ] | bool[ wiiset.BTN_B ] :
    return True
  elif bool[ wiiset.BTN_1 ] | bool[ wiiset.BTN_2 ] :
    return True
  elif bool[ wiiset.BTN_MIN ] | bool[ wiiset.BTN_HOME ] | bool[ wiiset.BTN_PLUS ] :
    return True
  return False

def resetButtons():
  w = wiiset
  b = wiiset.BOOLS
  if ( ( not w.IsEiking() ) & ( w.eiking == True ) ) :
    w.eiking = False
  b[w.BTN_1] = 0
  b[w.BTN_2] = 0
  b[w.BTN_A] = 0
  b[w.BTN_B] = 0
  b[w.BTN_MIN] = 0
  b[w.BTN_HOME] = 0
  b[w.BTN_PLUS] = 0
  b[w.BTN_UP] = 0
  b[w.BTN_DOWN] = 0
  b[w.BTN_LEFT] = 0
  b[w.BTN_RIGHT] = 0
