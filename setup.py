import os 
import time

tempfolder = "/home/pi/temp"
pathgit = "/home/pi/git"
pathusbfile = "/media/setup.py"
pathlauncherfile = pathgit + "/wiiexpresspi/launcher.py"
scriptpath1 = "sudo python " + tempfolder + "/setup.py"
scriptpath2 = "sudo python " + pathlauncherfile
sleeptime = .5
charstart = 2
printchar = '-'
printlength = 80

def setup():
  Title( 'Start Setup' )
  updateDpkg()
  update()
  upgrade()
  succes = True
  if ( os.path.isdir( tempfolder ) == False ):
    checkPip()
    succes =  updateDpkg()
    if ( succes ):
      succes = editStartupScript()
    if ( succes ):
      succes = editStartupFiles()
  else:
    if ( succes ):
      succes = editStartupScript()
  if ( succes ):
    succes = updateDpkg()
  if ( succes ):
    succes = checkBluetooth()
  if ( succes ):
    succes = checkGit()
  if ( succes ):
    succes = succes & checkWiiexpresspi()
  if ( succes ):
    succes = succes & checkGpio()
  if ( succes ):
    succes = succes & checkCwiid()
  removeTempFiles()
  if ( succes ):
    return 0
  return 1

def update():
  Title( 'Update' )
  result = os.system( "sudo apt-get update" )
  Line()
  Message( 'Update - up-to-date' )
  Line()

def upgrade():
  Title( 'Upgrade' )
  result = os.system( "sudo apt-get -y upgrade" )
  Line()
  Message( 'Upgrade - up-to-date' )
  Line()

def checkPip():
  Title( 'Pip' )
  result = os.system( "sudo apt-get -y install python-dev python-pip" )
  if ( result == 0 ):
    return True
  return False

def updateDpkg():
  Title( 'DPKG' )
  result = os.system( "sudo dpkg --configure -a" )
  if ( result == 0 ):
    return True
  return False

def editStartupScript():
  Title( 'Auto Script' )
  filepath = "/etc/profile"

  try:
    with open( filepath, 'r' ) as file:
      data = file.readlines()
    contains = False
    for i in range( 0, len( data ) ):
      line = data[ i ]
      if ( "/wiiexpress" in line ):
        Message( 'Script is OK' )
        return True
      elif ( "/temp" in line ):
        Message( 'Renavigate profile to launch file' )
        data[ i ] = scriptpath2
        with open( filepath, 'w' ) as file:
          file.writelines( data )
        contains = True
    if ( contains == False ):
      Message( 'Placing data...' )
      if ( os.path.isdir( tempfolder ) == True ):
        os.system( "sudo rm -r " + tempfolder )
      result = os.mkdir( tempfolder )
      result = os.system( "cp " + pathusbfile + " " + tempfolder )
      data[ len( data ) - 1 ] = data[ len( data ) - 1 ] + "\n" + scriptpath1 + "\n"
      with open( filepath, 'w' ) as file:
        file.writelines( data )
  except:
    Message( 'except' )
    return False
  return True

def editStartupFiles():
  Title( 'Auto Startup' )
  filepath = "/etc/inittab"
  scriptline = "1:2345:respawn:/bin/login -f pi tty1 </dev/tty1 >/dev/tty1 2>&1"

  try:
    with open( filepath, 'r') as file:
      data = file.readlines()
    ischanged = False
    for i in range( 0, len( data ) ):
      ischanged = False
      if ( data[i].startswith( "1:23:respawn:/sbin/getty" ) == True ):
        ischanged = True
      elif ( data[i].startswith( "1:2345:respawn:/sbin/getty" ) == True):
        ischanged = True
      elif ( data[i].startswith( "1:2345:respawn:/bin/login" ) == True):
        break;
  
      if ( ischanged ):
        Message( 'Changing data...' )
        newline = "#" + data[i] + scriptline + "\n"
        data[i] = newline
        with open( filepath, 'w' ) as file:
          file.writelines( data )
        Shutdown()
        return False
    Message( 'Allready Changed' )
  except:
    Message( 'except' )
    return False
  return True

def checkBluetooth():
  Title( 'Bluetooth' )
  result = os.system( "sudo service bluetooth status" )
  if ( result != 0 ):
    return bluetooth()
  return True

def bluetooth():
  result = os.system( "sudo apt-get -y install --no-install-recommends bluetooth" )
  return bluetoothTest()

def bluetoothTest():
  Title( 'Bluetooth test' )
  Line()
  Message( "Bluetooth status should be: 'bluetooth is running'" )
  Line()
  result = os.system( "sudo service bluetooth status" )
  if ( result != 0 ):
    Warning( 'Bluetooth installation failed! [code: ' + str( result ) + ']' )
    return False
  return True

def checkGit():
  Title( 'Git' )
  if ( os.path.isdir( pathgit ) == False ):
    os.chdir( "/home/pi" )
    os.mkdir( "git" )
    Message( 'Git directory has been created' )
  return git()

def git():
  result = os.system( "git version" )
  if ( result == 0 ):
    Line()
    Message( 'Git allready installed' )
    Line()
  else:
    result = os.system( "sudo apt-get install git-core" )
    if ( result != 0 ):
      Warning( 'Git install failed!' )
      return False
  return True

def checkWiiexpresspi():
  Title( 'WiiExpressPi' )
  if ( os.path.isdir( pathgit + "wiiexpresspi" ) == False ):
    return wiiexpresspi()
  Line()
  Message( 'Wii Express Pi Allready Pulled' )
  Line()
  return True

def wiiexpresspi():
  os.chdir( pathgit )
  result = os.system( "sudo git clone https://ICTRoelSuntjens@bitbucket.com/ICTRoelSuntjens/wiiexpresspi.git" )
  if ( result != 0 | os.path.isdir( pathgit + "/wiiexpresspi" ) == False ):
    Warning( 'Cloning wiiexpresspi from bitbucket failed' )
    return False
  return True

def checkGpio():
  Title( 'GPIO' )
  if ( os.path.isdir( pathgit + "/wiiexpresspi/wiringPi" ) == False ):
    return gpio()
  Message( 'Wiring Pi Allready Pulled' )
  return True

def gpio():
  if ( os.path.isdir( pathgit + "/wiiexpresspi" ) == True ):
    os.chdir( pathgit + "/wiiexpresspi/" )
    result = os.system( "sudo pip install wiringpi2" )
    if ( result != 0 ):
      Warning( 'Install wiringpi2 failed!' )
      return False
  else:
    Warning( 'GPIO Failed!' )
    return False
  return True

def checkCwiid():
  Title( 'Cwiid' )
  result = os.system( "sudo apt-get install python-cwiid" )
  return True

def removeTempFiles():
  Title( 'Remove Temp Files' )
  if ( os.path.isdir( tempfolder ) == True ):
    result = os.system( "sudo rm -r " + tempfolder + "/" )
    if ( result != 0 ):
      return False
    Message( 'Removed ' + tempfolder )
    return True
  Message( 'Remove Succesfull' )
  return False

def Message( content ):
  line1 = "".ljust(charstart, printchar) + ' '
  line2 = ' ' + "".ljust(printlength - len(content) - 4, printchar)
  line = line1 + content + line2
  print line

def Title( content ):
  Line()
  Message( content )
  Line()
  time.sleep( sleeptime )

def Warning( content ):
  MessageColor( content, '\033[33;1m' )

def Failed( content ):
  MessageColor( content, '\033[31;1m' )

def Succes( content ):
  MessageColor( content, '\033[32;1m' )

def MessageColor( content, color ):
  line1 = "".ljust(charstart, printchar) + ' '
  line2 = ' ' + "".ljust(printlength - len(content) - 4, printchar)
  line = line1 + color + content + '\033[0m' + line2
  print line

def Line():
  line = "".ljust(printlength, printchar)
  print line

def Shutdown():
  Line()
  Warning( 'Reboot is necessary' )
  Line()
  Warning( 'Reboot in 5 sec' )
  Line()
  time.sleep( 5 )
  os.system( "sudo reboot" )


def Launch():
  Title( 'Launch ' + pathlauncherfile )
  os.system( scriptpath2 )

if __name__ == '__main__':
  if ( setup() == 0 ):
    Line()
    Succes( 'Installation succesfull' )
    Title( 'End Setup' )
    Shutdown()
  else:
    Line()
    Failed( 'Installation aborted' )
    Title( 'End Setup' )
