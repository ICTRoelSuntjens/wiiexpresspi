# ----------------------------------------
# @title: ---- Log Manager ---------------
# @author: --- created by Roel Suntjens --
# @date: ----- 04/01/2016 ----------------
# @version: -- v1.0 ----------------------
# ----------------------------------------

import ledm
import wiiset

 
printlength = 40
printchar = '-'
charstart = 2

def init():
  charstart = 2
  return True

def Warning(content, stayOn):
  line1 = "".ljust(charstart, printchar) + ' '
  line2 = ' ' + "".ljust(printlength - len(content) - 4, printchar)
  line = line1 + wiiset.WARNING + content + wiiset.ENDC + line2
  Print(line)
  ledm.Warning(stayOn)

def Error(content, stayOn):
  line1 = "".ljust(charstart, printchar) + ' '
  line2 = ' ' + "".ljust(printlength - len(content) - 4, printchar)
  line = line1 + wiiset.FAILED + content + wiiset.ENDC + line2
  Print(line)
  ledm.Error(stayOn)

def Message(content):
  line = content
  if ( len(line) < printlength - 4 ) :
    line = printchar + printchar + ' ' + content + ' '
  Print(line)

def MessageColor(content, color):
  line = content
  if ( len(line) < printlength - 4 ) :
    line = color + printchar + printchar + ' ' + content + ' '
    line = line.ljust(printlength + 7, printchar)
  else :
    line = color + line
  Print ( line )

def MessageMiddle(content):
  Print(GetContentMiddle(content, wiiset.ENDC))

def MessageMiddleColor(content, color):
  Print(GetContentMiddle(content, color))

def GetContentMiddle(content, color):
  content = ' ' + content + ' '
  contentlength = len(content)
  line = "".ljust(printlength/2 - contentlength/2, printchar)
  line = line + color + content + wiiset.ENDC + line
  return line

def Beginning():
  print wiiset.TODO + ' log, beginning'

def Ending():
  print wiiset.TODO + ' log, ending'

def FillLine():
  Print("")

def FillLineColor(color):
  line = color + "".ljust(printlength, printchar) + wiiset.ENDC
  Print(line)

def PrintState(succes):
  if succes == 0 :
    MessageMiddleColor("Failed", wiiset.FAILED)
  else:
    MessageMiddleColor("Succes", wiiset.GREEN)

def Print(content):
  line = content.ljust(printlength, printchar) + wiiset.ENDC
  print line
